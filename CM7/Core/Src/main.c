/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "string.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#ifndef HSEM_ID_0
#define HSEM_ID_0 (0U) /* HW semaphore 0*/
#endif


#define TOGGLE_PIN(NAME) HAL_GPIO_TogglePin( NAME ## _GPIO_Port, NAME ## _Pin );

#define CONFIGURE_PIN_AS_OUTPUT(NAME) \
            { \
	            GPIO_InitTypeDef GPIO_InitStruct = {0}; \
                GPIO_InitStruct.Pin = NAME ## _Pin;\
                GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;\
                GPIO_InitStruct.Pull = GPIO_NOPULL;\
                GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;\
                HAL_GPIO_Init(NAME ## _GPIO_Port, &GPIO_InitStruct);\
            }

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
#if defined ( __ICCARM__ ) /*!< IAR Compiler */

#pragma location=0x30040000
ETH_DMADescTypeDef  DMARxDscrTab[ETH_RX_DESC_CNT]; /* Ethernet Rx DMA Descriptors */
#pragma location=0x30040060
ETH_DMADescTypeDef  DMATxDscrTab[ETH_TX_DESC_CNT]; /* Ethernet Tx DMA Descriptors */
#pragma location=0x30040200
uint8_t Rx_Buff[ETH_RX_DESC_CNT][ETH_MAX_PACKET_SIZE]; /* Ethernet Receive Buffers */

#elif defined ( __CC_ARM )  /* MDK ARM Compiler */

__attribute__((at(0x30040000))) ETH_DMADescTypeDef  DMARxDscrTab[ETH_RX_DESC_CNT]; /* Ethernet Rx DMA Descriptors */
__attribute__((at(0x30040060))) ETH_DMADescTypeDef  DMATxDscrTab[ETH_TX_DESC_CNT]; /* Ethernet Tx DMA Descriptors */
__attribute__((at(0x30040200))) uint8_t Rx_Buff[ETH_RX_DESC_CNT][ETH_MAX_PACKET_SIZE]; /* Ethernet Receive Buffer */

#elif defined ( __GNUC__ ) /* GNU Compiler */

ETH_DMADescTypeDef DMARxDscrTab[ETH_RX_DESC_CNT] __attribute__((section(".RxDecripSection"))); /* Ethernet Rx DMA Descriptors */
ETH_DMADescTypeDef DMATxDscrTab[ETH_TX_DESC_CNT] __attribute__((section(".TxDecripSection")));   /* Ethernet Tx DMA Descriptors */
uint8_t Rx_Buff[ETH_RX_DESC_CNT][ETH_MAX_PACKET_SIZE] __attribute__((section(".RxArraySection"))); /* Ethernet Receive Buffers */

#endif

ETH_TxPacketConfig TxConfig;
ADC_HandleTypeDef hadc1;

ETH_HandleTypeDef heth;

SPI_HandleTypeDef hspi4;

TIM_HandleTypeDef htim17;

PCD_HandleTypeDef hpcd_USB_OTG_FS;

/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ETH_Init(void);
static void MX_SPI4_Init(void);
static void MX_USB_OTG_FS_PCD_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM17_Init(void);
void StartDefaultTask(void *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
/* USER CODE BEGIN Boot_Mode_Sequence_0 */
  int32_t timeout;
/* USER CODE END Boot_Mode_Sequence_0 */

/* USER CODE BEGIN Boot_Mode_Sequence_1 */
  /* Wait until CPU2 boots and enters in stop mode or timeout*/
//  timeout = 0xFFFF;
//  while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) != RESET) && (timeout-- > 0));
//  if ( timeout < 0 )
//  {
//  Error_Handler();
//  }
/* USER CODE END Boot_Mode_Sequence_1 */
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();
/* USER CODE BEGIN Boot_Mode_Sequence_2 */
/* When system initialization is finished, Cortex-M7 will release Cortex-M4 by means of
HSEM notification */
/*HW semaphore Clock enable*/
__HAL_RCC_HSEM_CLK_ENABLE();
/*Take HSEM */
HAL_HSEM_FastTake(HSEM_ID_0);
/*Release HSEM in order to notify the CPU2(CM4)*/
HAL_HSEM_Release(HSEM_ID_0,0);
/* wait until CPU2 wakes up from stop mode */
timeout = 0xFFFF;
while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) == RESET) && (timeout-- > 0));
if ( timeout < 0 )
{
Error_Handler();
}
/* USER CODE END Boot_Mode_Sequence_2 */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ETH_Init();
  MX_SPI4_Init();
  MX_USB_OTG_FS_PCD_Init();
  MX_ADC1_Init();
  MX_TIM17_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_SMPS_1V8_SUPPLIES_LDO);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Macro to configure the PLL clock source
  */
  __HAL_RCC_PLL_PLLSOURCE_CONFIG(RCC_PLLSOURCE_HSE);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 5;
  RCC_OscInitStruct.PLL.PLLN = 48;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 5;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_2;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV1;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_SPI4|RCC_PERIPHCLK_ADC
                              |RCC_PERIPHCLK_USB;
  PeriphClkInitStruct.PLL2.PLL2M = 2;
  PeriphClkInitStruct.PLL2.PLL2N = 12;
  PeriphClkInitStruct.PLL2.PLL2P = 5;
  PeriphClkInitStruct.PLL2.PLL2Q = 2;
  PeriphClkInitStruct.PLL2.PLL2R = 2;
  PeriphClkInitStruct.PLL2.PLL2RGE = RCC_PLL2VCIRANGE_3;
  PeriphClkInitStruct.PLL2.PLL2VCOSEL = RCC_PLL2VCOMEDIUM;
  PeriphClkInitStruct.PLL2.PLL2FRACN = 0;
  PeriphClkInitStruct.Spi45ClockSelection = RCC_SPI45CLKSOURCE_D2PCLK1;
  PeriphClkInitStruct.UsbClockSelection = RCC_USBCLKSOURCE_PLL;
  PeriphClkInitStruct.AdcClockSelection = RCC_ADCCLKSOURCE_PLL2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable USB Voltage detector
  */
  HAL_PWREx_EnableUSBVoltageDetector();
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_16B;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ConversionDataManagement = ADC_CONVERSIONDATA_DR;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.LeftBitShift = ADC_LEFTBITSHIFT_NONE;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the ADC multi-mode
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  sConfig.OffsetSignedSaturation = DISABLE;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief ETH Initialization Function
  * @param None
  * @retval None
  */
static void MX_ETH_Init(void)
{

  /* USER CODE BEGIN ETH_Init 0 */

  /* USER CODE END ETH_Init 0 */

  /* USER CODE BEGIN ETH_Init 1 */

  /* USER CODE END ETH_Init 1 */
  heth.Instance = ETH;
  heth.Init.MACAddr[0] =   0x00;
  heth.Init.MACAddr[1] =   0x80;
  heth.Init.MACAddr[2] =   0xE1;
  heth.Init.MACAddr[3] =   0x00;
  heth.Init.MACAddr[4] =   0x00;
  heth.Init.MACAddr[5] =   0x00;
  heth.Init.MediaInterface = HAL_ETH_MII_MODE;
  heth.Init.TxDesc = DMATxDscrTab;
  heth.Init.RxDesc = DMARxDscrTab;
  heth.Init.RxBuffLen = 1524;

  /* USER CODE BEGIN MACADDRESS */

  /* USER CODE END MACADDRESS */

  if (HAL_ETH_Init(&heth) != HAL_OK)
  {
    Error_Handler();
  }

  memset(&TxConfig, 0 , sizeof(ETH_TxPacketConfig));
  TxConfig.Attributes = ETH_TX_PACKETS_FEATURES_CSUM | ETH_TX_PACKETS_FEATURES_CRCPAD;
  TxConfig.ChecksumCtrl = ETH_CHECKSUM_IPHDR_PAYLOAD_INSERT_PHDR_CALC;
  TxConfig.CRCPadCtrl = ETH_CRC_PAD_INSERT;
  /* USER CODE BEGIN ETH_Init 2 */

  /* USER CODE END ETH_Init 2 */

}

/**
  * @brief SPI4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI4_Init(void)
{

  /* USER CODE BEGIN SPI4_Init 0 */

  /* USER CODE END SPI4_Init 0 */

  /* USER CODE BEGIN SPI4_Init 1 */

  /* USER CODE END SPI4_Init 1 */
  /* SPI4 parameter configuration*/
  hspi4.Instance = SPI4;
  hspi4.Init.Mode = SPI_MODE_MASTER;
  hspi4.Init.Direction = SPI_DIRECTION_2LINES;
  hspi4.Init.DataSize = SPI_DATASIZE_4BIT;
  hspi4.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi4.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi4.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi4.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi4.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi4.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi4.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi4.Init.CRCPolynomial = 0x0;
  hspi4.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  hspi4.Init.NSSPolarity = SPI_NSS_POLARITY_LOW;
  hspi4.Init.FifoThreshold = SPI_FIFO_THRESHOLD_01DATA;
  hspi4.Init.TxCRCInitializationPattern = SPI_CRC_INITIALIZATION_ALL_ZERO_PATTERN;
  hspi4.Init.RxCRCInitializationPattern = SPI_CRC_INITIALIZATION_ALL_ZERO_PATTERN;
  hspi4.Init.MasterSSIdleness = SPI_MASTER_SS_IDLENESS_00CYCLE;
  hspi4.Init.MasterInterDataIdleness = SPI_MASTER_INTERDATA_IDLENESS_00CYCLE;
  hspi4.Init.MasterReceiverAutoSusp = SPI_MASTER_RX_AUTOSUSP_DISABLE;
  hspi4.Init.MasterKeepIOState = SPI_MASTER_KEEP_IO_STATE_DISABLE;
  hspi4.Init.IOSwap = SPI_IO_SWAP_DISABLE;
  if (HAL_SPI_Init(&hspi4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI4_Init 2 */

  /* USER CODE END SPI4_Init 2 */

}

/**
  * @brief TIM17 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM17_Init(void)
{

  /* USER CODE BEGIN TIM17_Init 0 */

  /* USER CODE END TIM17_Init 0 */

  /* USER CODE BEGIN TIM17_Init 1 */

  /* USER CODE END TIM17_Init 1 */
  htim17.Instance = TIM17;
  htim17.Init.Prescaler = 64-1;
  htim17.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim17.Init.Period = 65535;
  htim17.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim17.Init.RepetitionCounter = 0;
  htim17.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim17) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM17_Init 2 */

  /* USER CODE END TIM17_Init 2 */

}

/**
  * @brief USB_OTG_FS Initialization Function
  * @param None
  * @retval None
  */
static void MX_USB_OTG_FS_PCD_Init(void)
{

  /* USER CODE BEGIN USB_OTG_FS_Init 0 */

  /* USER CODE END USB_OTG_FS_Init 0 */

  /* USER CODE BEGIN USB_OTG_FS_Init 1 */

  /* USER CODE END USB_OTG_FS_Init 1 */
  hpcd_USB_OTG_FS.Instance = USB_OTG_FS;
  hpcd_USB_OTG_FS.Init.dev_endpoints = 9;
  hpcd_USB_OTG_FS.Init.speed = PCD_SPEED_FULL;
  hpcd_USB_OTG_FS.Init.dma_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.phy_itface = PCD_PHY_EMBEDDED;
  hpcd_USB_OTG_FS.Init.Sof_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.low_power_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.lpm_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.battery_charging_enable = ENABLE;
  hpcd_USB_OTG_FS.Init.vbus_sensing_enable = ENABLE;
  hpcd_USB_OTG_FS.Init.use_dedicated_ep1 = DISABLE;
  if (HAL_PCD_Init(&hpcd_USB_OTG_FS) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USB_OTG_FS_Init 2 */

  /* USER CODE END USB_OTG_FS_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOK_CLK_ENABLE();
  __HAL_RCC_GPIOJ_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, IPMB_A_SCL_Pin|IPMB_A_SDA_Pin|DIMM_UART0_TX_Pin|PWR_GOOD_B_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOH, DIMM_UART0_RX_Pin|ETH_TX_ER_Pin|MASTER_TCK_Pin|PWR_GOOD_A_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, DIMM_UART1_TX_Pin|I2C3_SDA_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, DIMM_UART1_RX_Pin|ALARM_B_Pin|MASTER_TDO_Pin|EN_12V_Pin
                          |FP_LED_2_Pin|MASTER_TRST_Pin|FP_LED_0_Pin|FP_LED_1_Pin
                          |FP_LED_BLUE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(ETH_RST_N_GPIO_Port, ETH_RST_N_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(I2C3_SCL_GPIO_Port, I2C3_SCL_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOF, IPMB_B_SCL_Pin|IPMB_B_SDA_Pin|ALARM_A_Pin|MGM_SCL_Pin
                          |MGM_SDA_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, IPMB_B_EN_Pin|IPMB_A_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOJ, MASTER_TDI_Pin|MASTER_TMS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(PYLD_RESET_GPIO_Port, PYLD_RESET_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : I2C_BB_SCL_Pin I2C_BB_SDA_Pin */
  GPIO_InitStruct.Pin = I2C_BB_SCL_Pin|I2C_BB_SDA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_0_Pin IPM_1_Pin IPM_3_Pin IPM_4_Pin
                           IPM_5_Pin USR_IO_10_Pin */
  GPIO_InitStruct.Pin = IPM_0_Pin|IPM_1_Pin|IPM_3_Pin|IPM_4_Pin
                          |IPM_5_Pin|USR_IO_10_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pins : USR_IO_6_Pin USR_IO_27_Pin USR_IO_14_Pin HANDLE_Pin
                           USR_IO_12_Pin USR_IO_33_Pin */
  GPIO_InitStruct.Pin = USR_IO_6_Pin|USR_IO_27_Pin|USR_IO_14_Pin|HANDLE_Pin
                          |USR_IO_12_Pin|USR_IO_33_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : USR_IO_0_Pin USR_IO_1_Pin IPM_11_Pin USR_IO_23_Pin
                           USR_IO_25_Pin USR_IO_28_Pin USR_IO_29_Pin USR_IO_20_Pin
                           USR_IO_24_Pin USR_IO_22_Pin USR_IO_26_Pin */
  GPIO_InitStruct.Pin = USR_IO_0_Pin|USR_IO_1_Pin|IPM_11_Pin|USR_IO_23_Pin
                          |USR_IO_25_Pin|USR_IO_28_Pin|USR_IO_29_Pin|USR_IO_20_Pin
                          |USR_IO_24_Pin|USR_IO_22_Pin|USR_IO_26_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : IPMB_A_SCL_Pin IPMB_A_SDA_Pin DIMM_UART0_TX_Pin PYLD_RESET_Pin
                           PWR_GOOD_B_Pin */
  GPIO_InitStruct.Pin = IPMB_A_SCL_Pin|IPMB_A_SDA_Pin|DIMM_UART0_TX_Pin|PYLD_RESET_Pin
                          |PWR_GOOD_B_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : DIMM_UART0_RX_Pin ETH_TX_ER_Pin MASTER_TCK_Pin PWR_GOOD_A_Pin */
  GPIO_InitStruct.Pin = DIMM_UART0_RX_Pin|ETH_TX_ER_Pin|MASTER_TCK_Pin|PWR_GOOD_A_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pins : DIMM_UART1_TX_Pin I2C3_SDA_Pin */
  GPIO_InitStruct.Pin = DIMM_UART1_TX_Pin|I2C3_SDA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_14_Pin USR_IO_7_Pin IPM_7_Pin IPMB_B_RDY_Pin
                           USR_IO_21_Pin USR_IO_17_Pin */
  GPIO_InitStruct.Pin = IPM_14_Pin|USR_IO_7_Pin|IPM_7_Pin|IPMB_B_RDY_Pin
                          |USR_IO_21_Pin|USR_IO_17_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pins : DIMM_UART1_RX_Pin ALARM_B_Pin MASTER_TDO_Pin EN_12V_Pin
                           FP_LED_2_Pin MASTER_TRST_Pin FP_LED_0_Pin FP_LED_1_Pin
                           FP_LED_BLUE_Pin */
  GPIO_InitStruct.Pin = DIMM_UART1_RX_Pin|ALARM_B_Pin|MASTER_TDO_Pin|EN_12V_Pin
                          |FP_LED_2_Pin|MASTER_TRST_Pin|FP_LED_0_Pin|FP_LED_1_Pin
                          |FP_LED_BLUE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : USR_IO_34_Pin IPM_10_Pin USR_IO_4_Pin USR_IO_5_Pin
                           USR_IO_8_Pin USR_IO_9_Pin */
  GPIO_InitStruct.Pin = USR_IO_34_Pin|IPM_10_Pin|USR_IO_4_Pin|USR_IO_5_Pin
                          |USR_IO_8_Pin|USR_IO_9_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : ETH_INT_N_Pin */
  GPIO_InitStruct.Pin = ETH_INT_N_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(ETH_INT_N_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : ETH_RST_N_Pin */
  GPIO_InitStruct.Pin = ETH_RST_N_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(ETH_RST_N_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : I2C3_SCL_Pin */
  GPIO_InitStruct.Pin = I2C3_SCL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(I2C3_SCL_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_13_Pin IPM_8_Pin USR_IO_2_Pin */
  GPIO_InitStruct.Pin = IPM_13_Pin|IPM_8_Pin|USR_IO_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_2_Pin IPM_6_Pin IPM_12_Pin IPM_9_Pin
                           USR_IO_18_Pin USR_IO_19_Pin */
  GPIO_InitStruct.Pin = IPM_2_Pin|IPM_6_Pin|IPM_12_Pin|IPM_9_Pin
                          |USR_IO_18_Pin|USR_IO_19_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : IPMB_B_SCL_Pin IPMB_B_SDA_Pin ALARM_A_Pin MGM_SCL_Pin
                           MGM_SDA_Pin */
  GPIO_InitStruct.Pin = IPMB_B_SCL_Pin|IPMB_B_SDA_Pin|ALARM_A_Pin|MGM_SCL_Pin
                          |MGM_SDA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : IPMB_B_EN_Pin IPMB_A_EN_Pin */
  GPIO_InitStruct.Pin = IPMB_B_EN_Pin|IPMB_A_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pin : IPMB_A_RDY_Pin */
  GPIO_InitStruct.Pin = IPMB_A_RDY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(IPMB_A_RDY_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : HW_5_Pin */
  GPIO_InitStruct.Pin = HW_5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(HW_5_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : HW_7_Pin */
  GPIO_InitStruct.Pin = HW_7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(HW_7_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : HW_3_Pin HW_1_Pin HW_6_Pin HW_4_Pin */
  GPIO_InitStruct.Pin = HW_3_Pin|HW_1_Pin|HW_6_Pin|HW_4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);

  /*Configure GPIO pins : IPM_15_Pin USR_IO_11_Pin USR_IO_31_Pin USR_IO_15_Pin
                           USR_IO_13_Pin USR_IO_16_Pin */
  GPIO_InitStruct.Pin = IPM_15_Pin|USR_IO_11_Pin|USR_IO_31_Pin|USR_IO_15_Pin
                          |USR_IO_13_Pin|USR_IO_16_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);

  /*Configure GPIO pins : MASTER_TDI_Pin MASTER_TMS_Pin */
  GPIO_InitStruct.Pin = MASTER_TDI_Pin|MASTER_TMS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);

  /*Configure GPIO pins : USR_IO_3_Pin USR_IO_32_Pin USR_IO_30_Pin */
  GPIO_InitStruct.Pin = USR_IO_3_Pin|USR_IO_32_Pin|USR_IO_30_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pins : HW_2_Pin HW_0_Pin */
  GPIO_InitStruct.Pin = HW_2_Pin|HW_0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN 5 */

  CONFIGURE_PIN_AS_OUTPUT(   ALARM_A          );
  CONFIGURE_PIN_AS_OUTPUT(   ALARM_B          );
  CONFIGURE_PIN_AS_OUTPUT(   DIMM_UART0_RX    );
  CONFIGURE_PIN_AS_OUTPUT(   DIMM_UART0_TX    );
  CONFIGURE_PIN_AS_OUTPUT(   DIMM_UART1_RX    );
  CONFIGURE_PIN_AS_OUTPUT(   DIMM_UART1_TX    );
  CONFIGURE_PIN_AS_OUTPUT(   EN_12V           );
  //CONFIGURE_PIN_AS_OUTPUT(   ETH_INT_N        );  MUST BE INPUT      NOT ACCESSIBLE
  //CONFIGURE_PIN_AS_OUTPUT(   ETH_RST_N        );  NOT ACCESSIBLE
  //CONFIGURE_PIN_AS_OUTPUT(   ETH_TX_ER        );  MUST BE INPUT      NOT ACCESSIBLE
  CONFIGURE_PIN_AS_OUTPUT(   FP_LED_0         );
  CONFIGURE_PIN_AS_OUTPUT(   FP_LED_1         );
  CONFIGURE_PIN_AS_OUTPUT(   FP_LED_2         );
  CONFIGURE_PIN_AS_OUTPUT(   FP_LED_BLUE      );
  //CONFIGURE_PIN_AS_OUTPUT(   HANDLE           );  MUST BE INPUT
  CONFIGURE_PIN_AS_OUTPUT(   HW_0             );
  CONFIGURE_PIN_AS_OUTPUT(   HW_1             );
  CONFIGURE_PIN_AS_OUTPUT(   HW_2             );
  CONFIGURE_PIN_AS_OUTPUT(   HW_3             );
  CONFIGURE_PIN_AS_OUTPUT(   HW_4             );
  CONFIGURE_PIN_AS_OUTPUT(   HW_5             );
  CONFIGURE_PIN_AS_OUTPUT(   HW_6             );
  CONFIGURE_PIN_AS_OUTPUT(   HW_7             );
  CONFIGURE_PIN_AS_OUTPUT(   I2C_BB_SCL       );
  CONFIGURE_PIN_AS_OUTPUT(   I2C_BB_SDA       );
  CONFIGURE_PIN_AS_OUTPUT(   I2C3_SCL         );
  CONFIGURE_PIN_AS_OUTPUT(   I2C3_SDA         );
  //CONFIGURE_PIN_AS_OUTPUT(   IPMB_A_EN        );  NOT ACCESSIBLE
  //CONFIGURE_PIN_AS_OUTPUT(   IPMB_A_RDY       );  MUST BE INPUT      NOT ACCESSIBLE
  CONFIGURE_PIN_AS_OUTPUT(   IPMB_A_SCL       );
  CONFIGURE_PIN_AS_OUTPUT(   IPMB_A_SDA       );
  //CONFIGURE_PIN_AS_OUTPUT(   IPMB_B_EN        );  NOT ACCESSIBLE
  //CONFIGURE_PIN_AS_OUTPUT(   IPMB_B_RDY       );  MUST BE INPUT      NOT ACCESSIBLE
  CONFIGURE_PIN_AS_OUTPUT(   IPMB_B_SCL       );
  CONFIGURE_PIN_AS_OUTPUT(   IPMB_B_SDA       );
  CONFIGURE_PIN_AS_OUTPUT(   MASTER_TRST      );
  CONFIGURE_PIN_AS_OUTPUT(   MASTER_TMS       );
  CONFIGURE_PIN_AS_OUTPUT(   MASTER_TCK       );
  CONFIGURE_PIN_AS_OUTPUT(   MASTER_TDI       );
  CONFIGURE_PIN_AS_OUTPUT(   MASTER_TDO       );
  CONFIGURE_PIN_AS_OUTPUT(   MGM_SCL          );
  CONFIGURE_PIN_AS_OUTPUT(   MGM_SDA          );
  CONFIGURE_PIN_AS_OUTPUT(   PWR_GOOD_A       );
  CONFIGURE_PIN_AS_OUTPUT(   PWR_GOOD_B       );
  CONFIGURE_PIN_AS_OUTPUT(   PYLD_RESET       );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_0            );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_1            );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_2            );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_3            );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_4            );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_5            );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_6            );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_7            );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_8            );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_9            );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_10           );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_11           );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_12           );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_13           );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_14           );
  CONFIGURE_PIN_AS_OUTPUT(   IPM_15           );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_0         );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_1         );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_2         );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_3         );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_4         );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_5         );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_6         );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_7         );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_8         );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_9         );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_10        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_11        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_12        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_13        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_14        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_15        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_16        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_17        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_18        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_19        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_20        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_21        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_22        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_23        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_24        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_25        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_26        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_27        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_28        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_29        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_30        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_31        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_32        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_33        );
  CONFIGURE_PIN_AS_OUTPUT(   USR_IO_34        );


  /* Infinite loop */
  for(;;)
  {
    osDelay(100);

    TOGGLE_PIN(   ALARM_A          );
    TOGGLE_PIN(   ALARM_B          );
    TOGGLE_PIN(   DIMM_UART0_RX    );
    TOGGLE_PIN(   DIMM_UART0_TX    );
    TOGGLE_PIN(   DIMM_UART1_RX    );
    TOGGLE_PIN(   DIMM_UART1_TX    );
    TOGGLE_PIN(   EN_12V           );
  //TOGGLE_PIN(   ETH_INT_N        );  MUST BE INPUT      NOT ACCESSIBLE
  //TOGGLE_PIN(   ETH_RST_N        );  NOT ACCESSIBLE
  //TOGGLE_PIN(   ETH_TX_ER        );  MUST BE INPUT      NOT ACCESSIBLE
    TOGGLE_PIN(   FP_LED_0         );
    TOGGLE_PIN(   FP_LED_1         );
    TOGGLE_PIN(   FP_LED_2         );
    TOGGLE_PIN(   FP_LED_BLUE      );
  //TOGGLE_PIN(   HANDLE           );  MUST BE INPUT
    TOGGLE_PIN(   HW_0             );
    TOGGLE_PIN(   HW_1             );
    TOGGLE_PIN(   HW_2             );
    TOGGLE_PIN(   HW_3             );
    TOGGLE_PIN(   HW_4             );
    TOGGLE_PIN(   HW_5             );
    TOGGLE_PIN(   HW_6             );
    TOGGLE_PIN(   HW_7             );
    TOGGLE_PIN(   I2C_BB_SCL       );
    TOGGLE_PIN(   I2C_BB_SDA       );
    TOGGLE_PIN(   I2C3_SCL         );
    TOGGLE_PIN(   I2C3_SDA         );
  //TOGGLE_PIN(   IPMB_A_EN        );  NOT ACCESSIBLE
  //TOGGLE_PIN(   IPMB_A_RDY       );  MUST BE INPUT      NOT ACCESSIBLE
    TOGGLE_PIN(   IPMB_A_SCL       );
    TOGGLE_PIN(   IPMB_A_SDA       );
  //TOGGLE_PIN(   IPMB_B_EN        );  NOT ACCESSIBLE
  //TOGGLE_PIN(   IPMB_B_RDY       );  MUST BE INPUT      NOT ACCESSIBLE
    TOGGLE_PIN(   IPMB_B_SCL       );
    TOGGLE_PIN(   IPMB_B_SDA       );
    TOGGLE_PIN(   MASTER_TRST      );
    TOGGLE_PIN(   MASTER_TMS       );
    TOGGLE_PIN(   MASTER_TCK       );
    TOGGLE_PIN(   MASTER_TDI       );
    TOGGLE_PIN(   MASTER_TDO       );
    TOGGLE_PIN(   MGM_SCL          );
    TOGGLE_PIN(   MGM_SDA          );
    TOGGLE_PIN(   PWR_GOOD_A       );
    TOGGLE_PIN(   PWR_GOOD_B       );
    TOGGLE_PIN(   PYLD_RESET       );
    TOGGLE_PIN(   IPM_0            );
    TOGGLE_PIN(   IPM_1            );
    TOGGLE_PIN(   IPM_2            );
    TOGGLE_PIN(   IPM_3            );
    TOGGLE_PIN(   IPM_4            );
    TOGGLE_PIN(   IPM_5            );
    TOGGLE_PIN(   IPM_6            );
    TOGGLE_PIN(   IPM_7            );
    TOGGLE_PIN(   IPM_8            );
    TOGGLE_PIN(   IPM_9            );
    TOGGLE_PIN(   IPM_10           );
    TOGGLE_PIN(   IPM_11           );
    TOGGLE_PIN(   IPM_12           );
    TOGGLE_PIN(   IPM_13           );
    TOGGLE_PIN(   IPM_14           );
    TOGGLE_PIN(   IPM_15           );
    TOGGLE_PIN(   USR_IO_0         );
    TOGGLE_PIN(   USR_IO_1         );
    TOGGLE_PIN(   USR_IO_2         );
    TOGGLE_PIN(   USR_IO_3         );
    TOGGLE_PIN(   USR_IO_4         );
    TOGGLE_PIN(   USR_IO_5         );
    TOGGLE_PIN(   USR_IO_6         );
    TOGGLE_PIN(   USR_IO_7         );
    TOGGLE_PIN(   USR_IO_8         );
    TOGGLE_PIN(   USR_IO_9         );
    TOGGLE_PIN(   USR_IO_10        );
    TOGGLE_PIN(   USR_IO_11        );
    TOGGLE_PIN(   USR_IO_12        );
    TOGGLE_PIN(   USR_IO_13        );
    TOGGLE_PIN(   USR_IO_14        );
    TOGGLE_PIN(   USR_IO_15        );
    TOGGLE_PIN(   USR_IO_16        );
    TOGGLE_PIN(   USR_IO_17        );
    TOGGLE_PIN(   USR_IO_18        );
    TOGGLE_PIN(   USR_IO_19        );
    TOGGLE_PIN(   USR_IO_20        );
    TOGGLE_PIN(   USR_IO_21        );
    TOGGLE_PIN(   USR_IO_22        );
    TOGGLE_PIN(   USR_IO_23        );
    TOGGLE_PIN(   USR_IO_24        );
    TOGGLE_PIN(   USR_IO_25        );
    TOGGLE_PIN(   USR_IO_26        );
    TOGGLE_PIN(   USR_IO_27        );
    TOGGLE_PIN(   USR_IO_28        );
    TOGGLE_PIN(   USR_IO_29        );
    TOGGLE_PIN(   USR_IO_30        );
    TOGGLE_PIN(   USR_IO_31        );
    TOGGLE_PIN(   USR_IO_32        );
    TOGGLE_PIN(   USR_IO_33        );
    TOGGLE_PIN(   USR_IO_34        );

  }
  /* USER CODE END 5 */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM7 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM7) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {

  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
